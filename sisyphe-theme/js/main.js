var items = document.querySelectorAll('li.item')
var list = document.querySelectorAll('ul.list')[0]
var sort = document.querySelectorAll('section.sort')[0]

function scrl(id) {
	var elmnt = document.getElementById(id);
	elmnt.scrollIntoView();
}

function shuffle(o) {
	var order = []
	for(var i = 0 ; i < o.length; i++) { order.push(i) }
	for(var j, x, i = order.length; i; j = parseInt(Math.random() * i), x = order[--i], order[i] = order[j], order[j] = x);
	return order
}

function activeList() {

	var openArticle = function(){

		var parentElem = this.parentElement 
		var next = this.nextElementSibling 
		var last = document.getElementsByClassName('last')[0]

		if (parentElem.getAttribute("data-active") == 'false'){
			parentElem.setAttribute('data-active', 'true')
			parentElem.classList.add('last')	
		} else if (parentElem.getAttribute("data-active") == 'true') {
			parentElem.setAttribute('data-active','false')
		}
		scrl(this.id)
	}
	var items = document.querySelectorAll('li.item')

	for (var i = 0; i < items.length; i++) {
		items[i].querySelector('.header').addEventListener('click', openArticle, false)

	}
}

function Intials(cString) {
	var sInitials = ""
	var wordArray = cString.split(" ")
	for( i = 0; i<wordArray.length; i++) {
		sInitials += wordArray[i].substring(0,1).toUpperCase()
	}
	return sInitials
}

function setCote(item) {

	var o = {
		type: item.getAttribute('data-type'),
		date: item.querySelector('.date').getAttribute('data-date'),
		title: item.querySelector('.title').innerHTML,
		categories: item.querySelector('.category').getAttribute('data-category'),
		authors: item.querySelector('.author').getAttribute('data-author')
	}

	var cote = '<span class="type_'+o.type+'">('+Intials(o.type)+')</span>_'+Intials(o.categories.replace('|', ' '))+'_'+Intials(o.title)+'_' + o.date
	// var cote = '<span class="type_'+o.type+'">('+Intials(o.type)+')</span>_'+Intials(o.categories.replace('|', ' '))+'_' + o.date
	return cote

}

function timeVisibility(elem, i) {
	setTimeout(function(){
		elem.classList.add('visible')
	}, i * 70, elem)
}

function getTagValues(items, types) {
	var t = []

	types.forEach(function(tt){ t[tt] = [] })

	items.forEach(function(item){
		var ch = item.children[0]
		types.forEach(function(tt){
			var el = ch.querySelector('.' + tt).getAttribute('data-' + tt).split('|')
			el.forEach(function(e){ t[tt].push(e) })
		})
	})

	return t
} 

function tagBuilder() {
	var type = getTagValues(items, ["category", "author"])
	var cats = [...new Set(type['category'])];
	var auts = [...new Set(type['author'])];

	var addTag = function(className, elem, d, m){
		var r = []
		r.push('<ul class="'+className+'-list" data-type="' + className + '" >')
		elem.forEach(function(e, i){
			e !== '' ? r.push('<li style="margin-' + d + ':' + ( m * i) +'px">' + e + '</li>') : false 
		})
		r.push('</ul>')
		return r.join('')
	}

	sort.innerHTML += addTag('category', cats, 'left', 1)
	sort.innerHTML += addTag('author', auts, 'right', 4)
} 

function spanEqual(className){
	var items = document.querySelectorAll(className)
	var max = 0
	var d = 10
	items.forEach(function(e){
		e.offsetWidth > max ? max = e.offsetWidth : max = max
	})
	items.forEach(function(e, i){
		e.style.width = max + (d * 5) + 'px'
	})
}

function sortA(key, value){

	var selec = []

	items.forEach(function(elem, i){
		elem.classList.remove('visible')
		var s = elem.querySelector('[data-'+key+']')
				var sv = s.getAttribute('data-'+key)
		sv.indexOf(value) > -1 ? selec.push(elem) : false

	})

	selec.forEach(function(elem, i){
		timeVisibility(elem, i)
	})
}

window.addEventListener('DOMContentLoaded', function(){
	var items = document.querySelectorAll('li.item')
	var itemsList = document.querySelector('ul.list')

	// tagBuilder()
	activeList()

	items.forEach(function(element, i){
		element.querySelector('.cote').innerHTML = setCote(items[i])
		timeVisibility(element, i)
	})

	var keysSort = document.querySelectorAll('.sort ul > li')

	keysSort.forEach(function(elem){
		elem.addEventListener('click', function(){
			console.log(elem)
			var key = this.parentElement.getAttribute('data-type')
			var value = this.innerHTML
			sortA(key, value)
		})
	})

	spanEqual('span.cote')
	// spanEqual('span.date')
	spanEqual('span.title')
	// spanEqual('span.category')
	// itemsList.style.visibility = 'visible'

})


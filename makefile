pathRepo=$(shell pwd)

install:
	wget https://getgrav.org/download/core/grav-admin/1.6.11/grav-admin-v1.6.11.zip
	unzip grav*.zip
	rm grav*.zip
	sudo chmod -R 777 grav-admin/
	ln -s $(pathRepo)/sisyphe-theme grav-admin/user/themes/sisyphe-theme
	rm -r $(pathRepo)/grav-admin/user/pages/
	cp -r common/pages/ $(pathRepo)/grav-admin/user/pages/
	rm -r $(pathRepo)/grav-admin/user/config/
	cp -r common/config/ $(pathRepo)/grav-admin/user/config/
	rm -r $(pathRepo)/grav-admin/user/accounts/
	cp -r common/accounts/ $(pathRepo)/grav-admin/user/accounts/


pre-push:
	rm -r common/accounts/
	cp -r $(pathRepo)/grav-admin/user/accounts/ common/accounts/
	rm -r common/pages/
	cp -r $(pathRepo)/grav-admin/user/pages/ common/pages/
	rm -r common/config/
	cp -r $(pathRepo)/grav-admin/user/config/  common/config/

post-pull:
	rm -r $(pathRepo)/grav-admin/user/pages/
	cp -r common/pages/ $(pathRepo)/grav-admin/user/pages/
	rm -r $(pathRepo)/grav-admin/user/config/
	cp -r common/config/ $(pathRepo)/grav-admin/user/config/
	rm -r $(pathRepo)/grav-admin/user/accounts/
	cp -r common/accounts/ $(pathRepo)/grav-admin/user/accounts/

---
title: 'May Swenson'
date: '23-05-2018 17:54'
taxonomy:
    category:
        - 'structure en bois'
        - évenement
    authors:
        - luc
        - Julliette
description: 'The advantage is that the special loop variable will count correctly thus not counting the users not iterated over. Keep in mind that properties like loop.last will not be defined when using loop conditions.The advantage is that the special loop variable will count correctly thus not counting the users not iterated over. Keep in mind that properties like loop.last will not be defined when using loop conditions.The advantage is that the special loop variable will count correctly thus not counting the users not iterated over. Keep in mind that properties like loop.last will not be defined when using loop conditions.The advantage is that the special loop variable will count correctly thus not counting the users not iterated over. Keep in mind that properties like loop.last will not be defined when using loop conditions.The advantage is that the special loop variable will count correctly thus not counting the users not iterated over. Keep in mind that properties like loop.last will not be defined when using loop conditions.'
imagesList:
    -
        media:
            user/pages/03.Blog/may-swenson/May_Swenson_1970_Feel_Me.jpg:
                name: May_Swenson_1970_Feel_Me.jpg
                type: image/jpeg
                size: 194408
                path: user/pages/03.Blog/may-swenson/May_Swenson_1970_Feel_Me.jpg
            user/pages/03.Blog/may-swenson/Hinton_et_al_2006_p_7.jpg:
                name: Hinton_et_al_2006_p_7.jpg
                type: image/jpeg
                size: 138033
                path: user/pages/03.Blog/may-swenson/Hinton_et_al_2006_p_7.jpg
---


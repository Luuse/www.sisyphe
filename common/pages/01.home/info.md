---
title: Sisyphe
adress: "rue du Chantier 2,\r\n1000 Bruxelles"
juridic: Asbl
corpnmbr: '718750538'
media_order: ''
body_classes: 'title-center title-h1h2'
order_by: ''
order_manual: ''
---

Né en Octobre 2018, Sisyphe a pour volonté de rassembler des énergies autour d’une dynamique commune de réhabilitation d’espaces abandonnés, vacants ou en processus de transformation en région bruxelloise, en les destinants à des activités artisanales, créatives, sociales et artistique.

La manière d’envisager ces espaces aujourd’hui à Bruxelles suit essentiellement une logique verticale et hiérarchique. Dans un contexte social et écologique en crise, nous pensons qu’en tant que citoyen bruxellois, nous pouvons être acteurs d’un devenir meilleur. Nous souhaitons nous immiscer dans cette structure verticale en mettant en place un réseau souple d’interactions artistiques et sociales, de rencontres et de débats. Nos projets se veulent optimistes, ouverts et sont orientés vers le public spontané de la ville. Leur particularité commune est d’agir dans ces bâtiments vacants en intégrant la population locale dans leur processus créatifs.
Ils s’expriment au travers de la réhabilitation d’espaces avec des matériaux de récupération, de la réalisation de structures construites, de mobilier, de dispositifs légers, de l’organisation de rencontres ou de conférences et d’ateliers d’apprentissage.

L’objet et l’intérêt de ces expérimentations urbaines n’est pas seulement dans le résultat, mais surtout dans le processus qui le génère et dans le nouvel environnement et les nouveaux comportements qu’il engendre.

Sisyphe est une association sans but lucratif fondée en Octobre 2018. Elle est composée de six acteurs issus de l’urbanisme, de l’architecture, du design, du cinéma et de l’art. La structure expérimente des modes d’autogestion depuis ses débuts et tente de démontrer l’intérêt d’un fonctionnement démocratique horizontal.
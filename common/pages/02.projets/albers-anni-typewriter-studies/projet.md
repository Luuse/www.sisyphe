---
title: 'Albers Anni Typewriter Studies'
date: '23-08-2019 15:09'
taxonomy:
    category:
        - workshop
    authors:
        - Jean-Marc
        - Marguerite
        - Alex
description: 'A quick word on style. I have none. 😃 I have casual way of talking and writing, and a strange sense of humour. I also tend to use random pop-culture references a lot, but often in ways that make no sense. If this is a problem you are entitled to a refund of the money you paid to me to write this... '
imagesList:
    -
        media:
            user/pages/02.projets/albers-anni-typewriter-studies/Hendricks_Bici_Punctuation_Poems_1966.jpg:
                name: Hendricks_Bici_Punctuation_Poems_1966.jpg
                type: image/jpeg
                size: 246782
                path: user/pages/02.projets/albers-anni-typewriter-studies/Hendricks_Bici_Punctuation_Poems_1966.jpg
            user/pages/02.projets/albers-anni-typewriter-studies/Albers_Anni_nd_Typewriter_Studies.jpg:
                name: Albers_Anni_nd_Typewriter_Studies.jpg
                type: image/jpeg
                size: 411900
                path: user/pages/02.projets/albers-anni-typewriter-studies/Albers_Anni_nd_Typewriter_Studies.jpg
authors:
    -
        text: Claire
    -
        text: Jean-Luc
    -
        text: Martin
    -
        text: Julienne
---


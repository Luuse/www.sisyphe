---
title: hirsal-josef-grogerova-bohumila
date: '22-08-2019 16:36'
taxonomy:
    category:
        - nourriture
        - workshop
    authors:
        - Jean-Marc
        - Claire
        - Wilson
description: 'since the default pad_type is STR_PAD_RIGHT. using STR_PAD_BOTH were always favor in the right pad if the required number of padding characters can''t be evenly divided.'
---

# Error: Invalid Frontmatter

Path: `/home/antoine/Public/www.sisyphe-bak/grav-admin/user/pages/02.projets/hirsal-josef-grogerova-bohumila/projet.md`

**Failed to read /home/antoine/Public/www.sisyphe-bak/grav-admin/user/pages/02.projets/hirsal-josef-grogerova-bohumila/projet.md: A YAML file cannot contain tabs as indentation at line 4 (near "		text: Jean-Luc").**

```
---
title: 'Hirsal Josef Grogerova Bohumila'
date: '16-07-2019 14:38'
authors:
			text: Jean-Luc
taxonomy:
    category:
        - typographie
        - construction
description: 'Sometimes it is not always clear just how many little shortcuts and features there are in Terminator. This manual hopes to reduce the confusion.'
imagesList:
    -
        media:
            user/pages/02.projets/hirsal-josef-grogerova-bohumila/Hirsal_Josef_Grogerova_Bohumila_JOB-BOJ_1968.jpg:
                name: Hirsal_Josef_Grogerova_Bohumila_JOB-BOJ_1968.jpg
                type: image/jpeg
                size: 233262
                path: user/pages/02.projets/hirsal-josef-grogerova-bohumila/Hirsal_Josef_Grogerova_Bohumila_JOB-BOJ_1968.jpg
taxonomies:
    category:
        - typographie
---


```